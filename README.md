# Introduction to Node.js

edX - Linux Foundation : Introduction to Node.js

## Command

npm install -g serve

The -g flag instructs the npm client to install the serve package globally, which means we can run the serve command at any time in our terminal.

To serve a folder we run serve {path} where {path} is a path to the folder we want to serve.

To try this out, let's create a new folder and then place some files inside it. Let's create a folder called static - we can do so with the following command:

node -e "fs.mkdirSync('static')"

```bash
serve {foldername}
```
